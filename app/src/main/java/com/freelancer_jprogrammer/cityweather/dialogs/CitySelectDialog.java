package com.freelancer_jprogrammer.cityweather.dialogs;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.DialogFragment;
import android.app.ProgressDialog;
import android.content.Context;
import android.os.AsyncTask;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.Spinner;

import com.freelancer_jprogrammer.cityweather.R;
import com.freelancer_jprogrammer.cityweather.activities.MainActivity;
import com.freelancer_jprogrammer.cityweather.interfaces.ISelectedData;
import com.freelancer_jprogrammer.cityweather.model.City;
import com.freelancer_jprogrammer.cityweather.utils.WeatherUtils;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.Collections;
import java.util.LinkedList;
import java.util.TreeSet;

/**
 * Created by Muhammad on 12/20/2016.
 */

public class CitySelectDialog extends DialogFragment{

    private ISelectedData mCallback;

    private Spinner continentSpinner;
    private Spinner countrySpinner;

    private LinkedList<City> countryCities;
    private LinkedList<City> resultCities;

    private EditText searchCity;
    private ListView citySearchListView;

    private ProgressDialog progressDialog;

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {

        final View dialogView = getActivity().getLayoutInflater().
                inflate(R.layout.dialog_city_select, null, false);

        countryCities = new LinkedList<>();
        resultCities = new LinkedList<>();

        progressDialog = new ProgressDialog( getActivity() );
        progressDialog.setMessage( "Loading......" );
        progressDialog.setCanceledOnTouchOutside( false );

        searchCity = (EditText) dialogView.findViewById( R.id.searchCity );
        searchCity.addTextChangedListener(
                new TextWatcher() {
                    @Override
                    public void beforeTextChanged(CharSequence s, int start, int count, int after) {

                    }

                    @Override
                    public void onTextChanged(CharSequence s, int start, int before, int count) {

                        searchAndLoadListView( s.toString() );
                    }

                    @Override
                    public void afterTextChanged(Editable s) {

                    }
                }
        );
        citySearchListView = (ListView) dialogView.findViewById( R.id.citySearchListView );
        citySearchListView.setOnItemClickListener(
                new AdapterView.OnItemClickListener() {
                    @Override
                    public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                        mCallback.onSelectedData( resultCities.get( i ) );
                        CitySelectDialog.this.dismiss();
                    }
                }
        );

        continentSpinner = (Spinner) dialogView.findViewById( R.id.continentSpinner );
        countrySpinner = (Spinner) dialogView.findViewById( R.id.countrySpinner );


        ArrayAdapter<String> continentSpinnerAdapter =
                new ArrayAdapter<String>( this.getActivity(), R.layout.spinner_view,
                        WeatherUtils.getContinents());
        continentSpinnerAdapter.setDropDownViewResource( R.layout.spinner_view );
        continentSpinner.setAdapter( continentSpinnerAdapter );
        continentSpinner.setOnItemSelectedListener(
                new AdapterView.OnItemSelectedListener() {
                    @Override
                    public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                        loadCountries();
                    }

                    @Override
                    public void onNothingSelected(AdapterView<?> adapterView) {

                    }
                }
        );

        countrySpinner.setOnItemSelectedListener(
                new AdapterView.OnItemSelectedListener() {
                    @Override
                    public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                        loadCities();
                    }

                    @Override
                    public void onNothingSelected(AdapterView<?> adapterView) {

                    }
                }
        );

        loadCountries();
        //loadCities();

        AlertDialog dialog = new AlertDialog.Builder( getActivity() )
                .setView( dialogView )
                .setTitle("SELECT A CITY")
                .setPositiveButton( "Close", null )
                .show();

        dialog.getButton(AlertDialog.BUTTON_POSITIVE ).setOnClickListener(
                new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        dismiss();
                    }
                }
        );

        if( dialog.isShowing() )
        {
            loadCities();
        }

        return dialog;
    }

    private void searchAndLoadListView(String searchString) {
        resultCities.clear();
        if( searchString.trim().equals( "" ) )
        {
            resultCities.addAll( countryCities );
        }
        else
        {
            for( City city : countryCities )
            {
                if( city.getName().toLowerCase().startsWith( searchString.toLowerCase() ) )
                {
                    resultCities.add( city );
                }
            }
        }
        loadListView();
    }

    private void loadListView() {
        if(  getActivity() != null )
        {
            ArrayAdapter<String> adapter = new ArrayAdapter<>(getActivity(),
                    android.R.layout.simple_list_item_1, WeatherUtils.getCityNames( resultCities ));
            citySearchListView.setAdapter( adapter );
        }
    }

    private void loadCountries()
    {
        String continent = ((String)continentSpinner.getSelectedItem()).trim();
        ArrayAdapter<String> countrySpinnerAdapter =
                new ArrayAdapter<String>( this.getActivity(), R.layout.spinner_view,
                        WeatherUtils.getSpecificCountries( continent ));
        countrySpinnerAdapter.setDropDownViewResource( R.layout.spinner_view );
        countrySpinner.setAdapter( countrySpinnerAdapter );
    }

    private void loadCities()
    {
        progressDialog.show();
        String continent = ((String)continentSpinner.getSelectedItem()).trim();
        String country = ((String)countrySpinner.getSelectedItem()).trim();
        GetCitiesTask getCitiesTask = new GetCitiesTask();
        LinkedList<Object> taskParams = new LinkedList<>();
        taskParams.add( country );
        taskParams.add( continent );
        taskParams.add( this.getActivity() );
        getCitiesTask.execute( taskParams );

        //countryCities = WeatherUtils.getCities( this.getActivity(), country, continent );
        //searchCity.setText( "" );
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        try {
            mCallback = (ISelectedData) context;
        }
        catch (ClassCastException e) {
            Log.d("MyDialog", "Activity doesn't implement the ISelectedData interface");
        }
    }

    private  class GetCitiesTask extends AsyncTask<LinkedList<Object>, Void, LinkedList<City>> {

        @Override
        protected LinkedList<City> doInBackground(LinkedList<Object>... strings) {
            String country = (String) strings[0].get(0);
            String continent = (String) strings[0].get(1);
            Context context = (Context) strings[0].get(2);
            String code = WeatherUtils.getCountries(context).get(continent).get(country);
            TreeSet<City> cities = new TreeSet<>();
            try {
                JSONArray jsonArray = new JSONArray(WeatherUtils.loadJSONFromAsset(context, "world-cities.json"));
                //JSONArray jsonArray = obj.getJSONArray( "" );
                for (int i = 0; i < jsonArray.length(); i++) {
                    JSONObject thisObject = jsonArray.getJSONObject(i);
                    if (thisObject.getString("country").equals(country)) {
                        City city = new City();
                        city.setSubCountry(thisObject.getString("subcountry"));
                        city.setContinent(continent);
                        city.setCountryCode(code);
                        city.setCountryName(country);
                        city.setName(thisObject.getString("name"));
                        cities.add(city);
                    }
                }
            } catch (Exception e) {
                e.printStackTrace();
            }

            LinkedList<City> resultCities = new LinkedList<>();
            resultCities.addAll(cities);
            CitySelectDialog.this.countryCities = resultCities;
            return null;
        }

        @Override
        protected void onPostExecute(LinkedList<City> cities) {
            progressDialog.dismiss();
            searchCity.setText( "" );
        }
    }
}
