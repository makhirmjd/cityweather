package com.freelancer_jprogrammer.cityweather.databases;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

/**
 * Created by Muhammad on 12/22/2016.
 */

public class PreferenceDatabase extends SQLiteOpenHelper {

    private static final String DB_NAME = "preference.sqlite";
    private static final int VERSION = 1;

    private static final String TABLE_PREFERENCE = "Preference";
    private static final String COLUMN_KEY = "Key";
    private static final String COLUMN_VALUE = "Value";
    private static final String[] REGISTRATION_COLUMNS = { COLUMN_KEY, COLUMN_VALUE };

    public PreferenceDatabase( Context context )
    {
        super(context, DB_NAME, null, VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase sqLiteDatabase) {

        sqLiteDatabase.execSQL( String.format( "DROP TABLE IF EXISTS %s",TABLE_PREFERENCE ) );

        sqLiteDatabase.execSQL(String.format( "CREATE TABLE %s (%s TEXT UNIQUE, %s TEXT )",
                TABLE_PREFERENCE, COLUMN_KEY, COLUMN_VALUE) );
    }

    @Override
    public void onUpgrade(SQLiteDatabase sqLiteDatabase, int i, int i1) {

    }

    //Start Setting Up Preference Table
    public long insertPreference( String key, String value )
    {
        ContentValues cv = new ContentValues();
        cv.put( COLUMN_KEY, key );
        cv.put( COLUMN_VALUE, value );
        return getWritableDatabase().insert( TABLE_PREFERENCE, null, cv );
    }

    public void updatePreference( String key, String value )
    {
        ContentValues cvUpdate = new ContentValues();
        cvUpdate.put( COLUMN_KEY, key );
        cvUpdate.put( COLUMN_VALUE, value );
        getWritableDatabase().update( TABLE_PREFERENCE, cvUpdate, COLUMN_KEY + "='" +
                key + "'", null );
    }

    public String getPreferenceValue( String key )
    {
        String valueToReturn = "";
        String[] columns = { COLUMN_KEY, COLUMN_VALUE };
        String query = String.format( "SELECT * FROM %s WHERE %s = '%s'", TABLE_PREFERENCE,
                COLUMN_KEY, key );
        Cursor cursor = getReadableDatabase().rawQuery( query, null );
        if( cursor.getCount() > 0 )
        {
            cursor.moveToFirst();
            valueToReturn = cursor.getString( cursor.getColumnIndex( columns[ 1 ] ) );
        }
        else
        {
            valueToReturn = null;
        }
        return  valueToReturn;
    }
}
