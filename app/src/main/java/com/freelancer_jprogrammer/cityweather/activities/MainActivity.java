package com.freelancer_jprogrammer.cityweather.activities;

import android.app.FragmentTransaction;
import android.app.ProgressDialog;
import android.content.Context;
import android.os.AsyncTask;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.freelancer_jprogrammer.cityweather.R;
import com.freelancer_jprogrammer.cityweather.adapters.WeatherArrayAdapter;
import com.freelancer_jprogrammer.cityweather.databases.PreferenceDatabase;
import com.freelancer_jprogrammer.cityweather.model.City;
import com.freelancer_jprogrammer.cityweather.model.Weather;
import com.freelancer_jprogrammer.cityweather.utils.WeatherUtils;
import com.freelancer_jprogrammer.cityweather.interfaces.ISelectedData;
import com.freelancer_jprogrammer.cityweather.dialogs.CitySelectDialog;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.LinkedList;
import java.util.List;
import java.util.TreeMap;
import java.util.TreeSet;

public class MainActivity extends AppCompatActivity implements ISelectedData {

    private List<Weather> weatherList = new ArrayList<>();

    private WeatherArrayAdapter weatherArrayAdapter;
    private ListView weatherListView;

    private City selectedCity;

    private TextView cityBanner;
    private TextView dateBanner;

    private ProgressDialog progressDialog;

    private FloatingActionButton fab;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        WeatherUtils.getTodaysDateString();
        WeatherUtils.getCountries( this );
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        cityBanner = (TextView) findViewById( R.id.cityBanner );
        dateBanner = (TextView) findViewById( R.id.stateBanner );

        weatherListView = (ListView) findViewById(R.id.weatherListView);
        weatherArrayAdapter = new WeatherArrayAdapter(this, weatherList);
        weatherListView.setAdapter(weatherArrayAdapter);

        progressDialog = new ProgressDialog( this );
        progressDialog.setMessage( "Forecasting....." );
        progressDialog.setCanceledOnTouchOutside( false );

        fab = (FloatingActionButton) findViewById( R.id.fab );
        fab.setOnClickListener(
                new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        if( selectedCity != null )
                        {
                            PreferenceDatabase database = new PreferenceDatabase( MainActivity.this );
                            String message = selectedCity.persist( database );
                            database.close();
                            Toast.makeText( MainActivity.this, message, Toast.LENGTH_SHORT ).show();
                        }
                        else
                        {
                            Toast.makeText( MainActivity.this, "No City Selected!", Toast.LENGTH_SHORT ).show();
                        }
                    }
                }
        );

        loadBanner();
        loadCityForeCast();
    }

    private URL createURL(String city) {
        String apiKey = getString(R.string.api_key);
        String baseUrl = getString(R.string.web_service_url);

        try {
            String urlString = baseUrl + URLEncoder.encode(city, "UTF-8") +
                    "&units=imperial&cnt=16&APPID=" + apiKey;
            return new URL(urlString);
        }
        catch (Exception e) {
            e.printStackTrace();
        }

        return null;
    }

    private class GetWeatherTask
            extends AsyncTask<URL, Void, JSONObject> {

        @Override
        protected JSONObject doInBackground(URL... params) {
            HttpURLConnection connection = null;

            try {
                connection = (HttpURLConnection) params[0].openConnection();
                int response = connection.getResponseCode();

                if (response == HttpURLConnection.HTTP_OK) {
                    StringBuilder builder = new StringBuilder();

                    try (BufferedReader reader = new BufferedReader(
                            new InputStreamReader(connection.getInputStream()))) {

                        String line;

                        while ((line = reader.readLine()) != null) {
                            builder.append(line);
                        }
                    } catch (IOException e) {
                        //Toast.makeText(MainActivity.this, R.string.read_error, Toast.LENGTH_SHORT).show();
                        e.printStackTrace();
                    }

                    return new JSONObject(builder.toString());
                } else {
                    Toast.makeText(MainActivity.this, R.string.connect_error, Toast.LENGTH_SHORT).show();
                }
            } catch (Exception e) {
                //Toast.makeText(MainActivity.this, R.string.connect_error, Toast.LENGTH_SHORT).show();
                e.printStackTrace();
            } finally {
                connection.disconnect();
            }

            return null;
        }

        @Override
        protected void onPostExecute(JSONObject weather) {
            convertJSONtoArrayList(weather);
            weatherArrayAdapter.notifyDataSetChanged();
            weatherListView.smoothScrollToPosition(0);
            progressDialog.dismiss();
        }

        private void convertJSONtoArrayList(JSONObject forecast) {
            weatherList.clear();
            if(forecast == null)
            {
                selectedCity = null;
                Toast.makeText(MainActivity.this, R.string.connect_error, Toast.LENGTH_SHORT).show();
                loadBanner();
                return;
            }

            try {
                JSONArray list = forecast.getJSONArray("list");

                for (int i = 0; i < list.length(); ++i) {
                    JSONObject day = list.getJSONObject(i);

                    JSONObject temperatures = day.getJSONObject("temp");


                    JSONObject weather =
                            day.getJSONArray("weather").getJSONObject(0);


                    weatherList.add(new Weather(
                            day.getLong("dt"),
                            temperatures.getDouble("min"),
                            temperatures.getDouble("max"),
                            day.getDouble("humidity"),
                            weather.getString("description"),
                            weather.getString("icon")));
                }
            }
            catch (JSONException e) {
                e.printStackTrace();
            }
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate( R.menu.menu_main, menu );
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch ( item.getItemId() )
        {
            case R.id.select_city_menu:
                FragmentTransaction transaction = getFragmentManager()
                        .beginTransaction()
                        .addToBackStack(null);
                CitySelectDialog dialog = new CitySelectDialog();
                dialog.show(transaction, null);
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onSelectedData(City city) {
        selectedCity = city;
        loadCityForeCast();
    }

    private void loadCityForeCast()
    {
        if( selectedCity != null )
        {
            URL url = createURL( selectedCity.getName() );
            if (url != null) {
                progressDialog.show();
                GetWeatherTask getLocalWeatherTask = new GetWeatherTask();
                getLocalWeatherTask.execute(url);
            }
            Log.e( "City", selectedCity.getName() );
            loadBanner();
        }
        else
        {
            PreferenceDatabase database = new PreferenceDatabase( this );
            selectedCity = City.getInstanceFromDatabase( database );
            database.close();
            if( selectedCity != null )
            {
                URL url = createURL( selectedCity.getName() );
                if (url != null) {
                    progressDialog.show();
                    GetWeatherTask getLocalWeatherTask = new GetWeatherTask();
                    getLocalWeatherTask.execute(url);
                }
                Log.e( "City", selectedCity.getName() );
                loadBanner();
            }
        }
    }

    private void loadBanner()
    {
        if( selectedCity != null )
        {
            cityBanner.setText( String.format( "%s, %s, %s", selectedCity.getName(), selectedCity.getSubCountry(), selectedCity.getCountryName() ) );
            dateBanner.setText( WeatherUtils.getTodaysDateString() );
        }
        else
        {
            cityBanner.setText( "Select City" );
            dateBanner.setText( "" );
        }
    }


}
