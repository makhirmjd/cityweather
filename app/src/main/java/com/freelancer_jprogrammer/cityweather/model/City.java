package com.freelancer_jprogrammer.cityweather.model;

import com.freelancer_jprogrammer.cityweather.databases.PreferenceDatabase;

import java.util.Comparator;

/**
 * Created by Muhammad on 12/19/2016.
 */

public class City implements Comparable<City>
{
    private String subCountry;
    private String name;
    private String countryCode;
    private String countryName;
    private String continent;

    public String getSubCountry() {
        return subCountry;
    }

    public void setSubCountry(String subCountry) {
        this.subCountry = subCountry;
    }

    public String getCountryCode() {
        return countryCode;
    }

    public void setCountryCode(String countryCode) {
        this.countryCode = countryCode;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getCountryName() {
        return countryName;
    }

    public void setCountryName(String countryName) {
        this.countryName = countryName;
    }

    public String getContinent() {
        return continent;
    }

    public void setContinent(String continent) {
        this.continent = continent;
    }

    @Override
    public String toString() {
        return "City{" +
                "subCountry='" + subCountry + '\'' +
                ", name='" + name + '\'' +
                ", countryCode='" + countryCode + '\'' +
                ", countryName='" + countryName + '\'' +
                ", continent='" + continent + '\'' +
                '}';
    }

    @Override
    public int compareTo(City city) {
        return name.compareTo( city.name );
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) return true;
        if (!(obj instanceof City)) return false;

        City that = (City)obj;
        return this.name.equalsIgnoreCase(that.name);
    }

    @Override
    public int hashCode() {
        return name.hashCode();
    }

    public String persist(PreferenceDatabase database )
    {
        String currentName = database.getPreferenceValue( "City" );
        String message = "";
        if( currentName == null )
        {
            database.insertPreference( "City", getCityPersistString() );
            message = String.format( "%s Saved As Default!", name );
        }
        else
        {
            City databaseCity = getInstanceFromDatabase( database );
            if( name.equals( databaseCity.getName() ) )
            {
                message = String.format( "%s Already Saved As Default!", name );
            }
            else
            {
                database.updatePreference( "City", getCityPersistString() );
                message = String.format( "%s Saved As Default!", name );
            }
        }

        return message;
    }

    public static City getInstanceFromDatabase( PreferenceDatabase database )
    {
        String currentName = database.getPreferenceValue( "City" );
        if( currentName == null )
        {
            return null;
        }
        String[] vals = currentName.split( "split" );
        City city = new City();
        city.setName( vals[ 0 ] );
        city.setSubCountry( vals[ 1 ] );
        city.setCountryName( vals[ 2 ] );
        city.setCountryCode( vals[ 3 ] );
        city.setContinent( vals[ 4 ] );

        return city;
    }

    private String getCityPersistString()
    {
        return String.format( "%ssplit%ssplit%ssplit%ssplit%s", name, subCountry, countryName, countryCode, continent );
    }
}
