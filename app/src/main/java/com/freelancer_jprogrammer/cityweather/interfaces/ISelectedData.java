package com.freelancer_jprogrammer.cityweather.interfaces;

import com.freelancer_jprogrammer.cityweather.model.City;

/**
 * Created by Muhammad on 12/20/2016.
 */

public interface ISelectedData {
    void onSelectedData(City city);
}
