package com.freelancer_jprogrammer.cityweather.utils;

import android.app.ProgressDialog;
import android.content.Context;
import android.os.AsyncTask;

import com.freelancer_jprogrammer.cityweather.model.City;

import org.json.JSONArray;
import org.json.JSONObject;

import java.io.IOException;
import java.io.InputStream;
import java.util.Calendar;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.TreeMap;
import java.util.TreeSet;

/**
 * Created by Muhammad on 12/19/2016.
 */

public class WeatherUtils {

    private static TreeMap<String, TreeMap<String, String>> countries;
    private static TreeMap<String, String> africa;
    private static TreeMap<String, String> antarctica;
    private static TreeMap<String, String> asia;
    private static TreeMap<String, String> europe;
    private static TreeMap<String, String> northAmerica;
    private static TreeMap<String, String> oceania;
    private static TreeMap<String, String> southAmerica;



    public static String loadJSONFromAsset(Context context, String fileName) {
        String json = null;
        try {
            InputStream is = context.getAssets().open(fileName);
            int size = is.available();
            byte[] buffer = new byte[size];
            is.read(buffer);
            is.close();
            json = new String(buffer, "UTF-8");
        } catch (IOException ex) {
            ex.printStackTrace();
            return null;
        }
        return json;
    }

    public static TreeMap<String, TreeMap<String, String>>  getCountries( Context context )
    {
        if( countries == null )
        {
            countries = new TreeMap<>();
            africa = new TreeMap<>();
            antarctica = new TreeMap<>();
            asia = new TreeMap<>();
            europe = new TreeMap<>();
            northAmerica = new TreeMap<>();
            oceania = new TreeMap<>();
            southAmerica = new TreeMap<>();
            try
            {
                JSONObject obj = new JSONObject( loadJSONFromAsset( context, "countries.json" ) );
                JSONObject jsonObject = obj.getJSONObject( "countries" );
                Iterator<String> keys = jsonObject.keys();
                while( keys.hasNext() )
                {
                    String key = (String)keys.next();
                    JSONObject thisObject = jsonObject.getJSONObject( key );
                    switch ( thisObject.getString( "continent" ) )
                    {
                        case "AF":
                            africa.put( thisObject.getString( "name" ), key );
                            break;
                        case "AN":
                            antarctica.put( thisObject.getString( "name" ), key );
                            break;
                        case "AS":
                            asia.put( thisObject.getString( "name" ), key );
                            break;
                        case "EU":
                            europe.put( thisObject.getString( "name" ), key );
                            break;
                        case "NA":
                            northAmerica.put( thisObject.getString( "name" ), key );
                            break;
                        case "OC":
                            oceania.put( thisObject.getString( "name" ), key );
                            break;
                        case "SA":
                            southAmerica.put( thisObject.getString( "name" ), key );
                            break;
                    }
                }

            }
            catch ( Exception e )
            {

            }
            countries.put( "Africa", africa );
            countries.put( "Antarctica", antarctica );
            countries.put( "Asia", asia );
            countries.put( "Europe", europe );
            countries.put( "North America", northAmerica );
            countries.put( "Oceania", oceania );
            countries.put( "South America", southAmerica );
        }

        return countries;
    }

    public static LinkedList<City> getCities( Context context, String country, String continent )
    {
        String code = getCountries( context ).get( continent ).get( country );
        TreeSet<City> cities = new TreeSet<>();
        try {
            JSONArray jsonArray = new JSONArray( loadJSONFromAsset( context, "world-cities.json" ) );
            //JSONArray jsonArray = obj.getJSONArray( "" );
            for ( int i = 0; i < jsonArray.length(); i++ )
            {
                JSONObject thisObject = jsonArray.getJSONObject( i );
                if( thisObject.getString( "country" ).equals( country )  )
                {
                    City city = new City();
                    city.setSubCountry( thisObject.getString( "subcountry" ) );
                    city.setContinent( continent );
                    city.setCountryCode( code );
                    city.setCountryName( country );
                    city.setName( thisObject.getString( "name" ) );
                    cities.add( city );
                }
            }
        }
        catch ( Exception e )
        {
            e.printStackTrace();
        }

        LinkedList<City> resultCities = new LinkedList<>();
        resultCities.addAll( cities );
        return resultCities;
    }

    /*public static LinkedList<City> getCities( Context context, String country, String continent )
    {
        progressDialog = new ProgressDialog( context );
        progressDialog.setMessage( "Loading......." );
        progressDialog.setCanceledOnTouchOutside( false );

        GetCitiesTask getCitiesTask = new GetCitiesTask();
        LinkedList<Object> objects = new LinkedList<>();
        objects.add( country );
        objects.add( continent );
        objects.add( context );
        LinkedList<City> cities = getCitiesTask.execute( objects );
    }*/

    public static String[] getContinents()
    {
        return new String[]{ "Africa", "Antarctica", "Asia", "Europe", "North America", "Oceania", "South America" };
    }

    public static String[] getSpecificCountries( String continent )
    {
        String[] countries = null;

        switch ( continent )
        {
            case "Africa":
                TreeSet<String> set = new TreeSet<>( africa.keySet() );
                LinkedList<String> sortedList = new LinkedList<>();
                sortedList.addAll( set );
                countries = sortedList.toArray( new String[ 0 ] );
                break;
            case "Antarctica":
                set = new TreeSet<>( antarctica.keySet() );
                sortedList = new LinkedList<>();
                sortedList.addAll( set );
                countries = sortedList.toArray( new String[ 0 ] );
                break;
            case "Asia":
                set = new TreeSet<>( asia.keySet() );
                sortedList = new LinkedList<>();
                sortedList.addAll( set );
                countries = sortedList.toArray( new String[ 0 ] );
                break;
            case "Europe":
                set = new TreeSet<>( europe.keySet() );
                sortedList = new LinkedList<>();
                sortedList.addAll( set );
                countries = sortedList.toArray( new String[ 0 ] );
                break;
            case "North America":
                set = new TreeSet<>( northAmerica.keySet() );
                sortedList = new LinkedList<>();
                sortedList.addAll( set );
                countries = sortedList.toArray( new String[ 0 ] );
                break;
            case "Oceania":
                set = new TreeSet<>( oceania.keySet() );
                sortedList = new LinkedList<>();
                sortedList.addAll( set );
                countries = sortedList.toArray( new String[ 0 ] );
                break;
            case "South America":
                set = new TreeSet<>( southAmerica.keySet() );
                sortedList = new LinkedList<>();
                sortedList.addAll( set );
                countries = sortedList.toArray( new String[ 0 ] );
                break;
        }

        return countries;
    }

    public static String[] getCityNames( LinkedList<City> cities )
    {
        String[] names = new String[ cities.size() ];
        for( int i = 0; i < cities.size(); i++ )
        {
            names[ i ] = cities.get( i ).getName();
        }

        return names;
    }

    public static String getTodaysDateString()
    {
        Calendar cal = Calendar.getInstance();
        String dayOfTheWeek = getDayOfTheWeek( cal );
        String date = String.format( "%02d/%02d/%04d",
                cal.get( Calendar.DAY_OF_MONTH ), cal.get( Calendar.MONTH ) + 1, cal.get( Calendar.YEAR )  );
        return String.format( "Today: %s, %s", dayOfTheWeek, date );
    }

    public static String getDayOfTheWeek( Calendar calendar )
    {
        String day = "";
        switch ( calendar.get( Calendar.DAY_OF_WEEK ) )
        {
            case 1:
                day = "Sunday";
                break;
            case 2:
                day = "Monday";
                break;
            case 3:
                day = "Tuesday";
                break;
            case 4:
                day = "Wednesday";
                break;
            case 5:
                day = "Thursday";
                break;
            case 6:
                day = "Friday";
                break;
            case 7:
                day = "Saturday";
                break;

        }

        return day;
    }

}
